import { Usuario } from './../shared/usuario';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url = 'http://localhost:8080/api/v1/usuarios';

  constructor(private http: HttpClient) { }

  getUsuarios() {
    return this.http.get(this.url);
  }

  postUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.url, usuario)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {

      if(error.error.mensagem){
        let errorMessage = error.error.mensagem;
        let errorCode = error.error.codigo;
        errorMessage = `Código do erro: ${errorCode}, ` + `menssagem: ${errorMessage}`;
        return throwError(errorMessage);
      }
    }

    console.log(error.message);
    return throwError('');
  };

}
