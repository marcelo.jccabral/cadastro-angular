import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioComponent } from './formulario.component';
import { FormBuilder } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Usuario } from '../shared/usuario';

let component: FormularioComponent;
  let fixture: ComponentFixture<FormularioComponent>;

describe('FormularioComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioComponent ],
      providers: [
        FormBuilder,
        HttpClient,
        HttpHandler
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(FormularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
