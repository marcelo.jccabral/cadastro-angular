import { UsuarioService } from './../service/usuario.service';
import { Component, OnInit } from '@angular/core';

import { Usuario } from '../shared/usuario';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
  animations: [
    trigger('fade', [
      state('void', style({ opacity: 1 })),
      state('*', style({ opacity: 0, display: 'none' })),
      transition(':enter, :leave', [
        animate(5000)
      ])
    ])
  ],
})
export class FormularioComponent implements OnInit {

  formUsuario!: FormGroup;
  usuarios : any;
  submitted = false;
  showErrorMessage = false;
  showSuccessMessage = false;
  mensagemErro = '';

  constructor(private formBuilder: FormBuilder, private usuarioService: UsuarioService) { }

  ngOnInit() {

    this.createForm(new Usuario());

    this.usuarioService.getUsuarios().subscribe(
      (response) => {
         this.usuarios = response;
         console.log(this.usuarios);
         this.showErrorMessage = false;
      },
      (error) => {
        console.log(error);
        this.showErrorMessage = true;
      });
  }

  createForm(usuario: Usuario) {
    this.formUsuario = this.formBuilder.group({
      nome: [usuario.nome, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      email: [usuario.email, [Validators.required, Validators.pattern('.+@.+\\..+')]],
      senha: [usuario.senha, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      confirmacaoSenha: [usuario.confirmacaoSenha, [Validators.required]]
    },
      {
        validator: (formGroup: FormGroup) => {
          const passwordControl = formGroup.controls['senha'];
          const confirmPasswordControl = formGroup.controls['confirmacaoSenha'];

          if (!passwordControl || !confirmPasswordControl) {
            return ;
          }

          if (confirmPasswordControl.errors && !confirmPasswordControl.errors['passwordMismatch']) {
            return ;
          }

          if (passwordControl.value !== confirmPasswordControl.value) {
            confirmPasswordControl.setErrors({ passwordMismatch: true });
          } else {
            confirmPasswordControl.setErrors(null);
          }
        },
      }
    )
  }

  get registerFormControl() {
    return this.formUsuario.controls;
  }

  onSubmit() {
    this.showErrorMessage = false;

    this.submitted = true;
    if (this.formUsuario.valid) {
        this.usuarioService.postUsuario(this.formUsuario.value).subscribe({
            next: () => {
              this.showSuccessMessage = true;
              this.ngOnInit();
              this.submitted = false;
            },
            error: (errorMessage) => {
              console.log(errorMessage);
              this.mensagemErro = errorMessage;
              this.showErrorMessage = true;
            },
          });
    }
  }

}
